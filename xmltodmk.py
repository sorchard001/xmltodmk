#!/usr/bin/python3

import argparse
import xml.etree.ElementTree as ET
from binascii import crc_hqx


# function to duplicate each value in byte array
# (single-density data in a dmk file has each byte repeated)
def double_bytes(data):
    op = [bb for b in data for bb in [b, b]]
    return bytes(op)

def make_index_mark():
    imark = b'\x4E' * 20
    imark += bytes(12)
    imark += b'\xC2\xC2\xC2\xFC'
    imark += b'\x4E' * 38
    return imark

def make_index_mark_sd():
    imark = b'\xFF' * 10
    imark += bytes(6)
    imark += b'\xFC'
    imark += b'\xFF' * 20
    return double_bytes(imark)

def make_idam(track, side, sector, size):
    idam = b'\xa1\xa1\xa1\xfe'
    idam += bytes([track, side, sector, size])
    idam += crc_hqx(idam, 0xffff).to_bytes(2, 'big')
    idam = bytes(12) + idam + b'\x4E' * 22
    return idam

def make_idam_sd(track, side, sector, size):
    idam = b'\xfe'
    idam += bytes([track, side, sector, size])
    idam += crc_hqx(idam, 0xffff).to_bytes(2, 'big')
    idam = bytes(6) + idam + b'\xFF' * 11
    return double_bytes(idam)

def make_sector(data):
    sec = b'\xa1\xa1\xa1\xfb'
    sec += data
    sec += crc_hqx(sec, 0xffff).to_bytes(2, 'big')
    sec = bytes(12) + sec + b'\x4E' * 22
    return sec

def make_sector_sd(data):
    sec = b'\xfb'
    sec += data
    sec += crc_hqx(sec, 0xffff).to_bytes(2, 'big')
    sec = bytes(6) + sec + b'\xFF' * 11
    return double_bytes(sec)


class DMKsector:
    def __init__(self, track_no, side, sector_id, datamark, data):
        self.track_no = track_no
        self.side = side
        self.id = sector_id
        self.datamark = datamark
        self.data = data


class DMKtrack:
    def __init__(self, side, double_density):
        self.side = side
        self.double_density = double_density
        self.sectors = []
    
    def addsector(self, track_no, side, sector_id, datamark, data):
        self.sectors.append(DMKsector(track_no, side, sector_id, datamark, data))


class DMKdisk:
    def __init__(self):
        self.tracks = []
        self.double_sided = False
    
    def addtrack(self, track):
        if len(track.sectors) > 0:
            self.tracks.append(track)
            if track.side > 0:
                self.double_sided = True
                
    def get_num_tracks(self):
        n = len(self.tracks)
        if self.double_sided:
            n = n // 2
        return n
    
    def from_xml_file(self, fname, verbose):
        tree = ET.parse(fname)
        root = tree.getroot()
        layout = root.find('layout')
        tracklist = layout.find('track_list')
    
        for track in tracklist:
            
            track_number = int(track.attrib['track_number'])
            side_number = int(track.attrib['side_number'])
            if verbose:
                print(track.tag, track_number, side_number)
            sectors = track.find('sector_list')
            format = track.find('format')
            dbl_density = False
            if not format is None:
                if verbose:
                    print(' ', format.text)
                if format.text == 'IBM_MFM':
                    dbl_density = True
        
            dmktrk = DMKtrack(side_number, dbl_density)
        
            secid_list = []
            
            for sector in sectors:
        
                sector_id = int(sector.attrib['sector_id'])
                sector_data = sector.find('sector_data')
                data_fill = sector.find('data_fill')
                datamark = sector.find('datamark')
        
                if not datamark is None:
                    datamark = int(datamark.text, 16).to_bytes(1, 'little')
        
                if not sector_data is None:
                    sector_payload = bytes.fromhex(sector_data.text)
                elif not data_fill is None:
                    sector_payload = bytearray([int(data_fill.text, 0)] * 256)
                else:
                    sector_payload = None
        
                if not sector_payload is None:
                    dmktrk.addsector(track_number, side_number, sector_id, datamark, sector_payload)
                    secid_list.append(sector_id)
        
            self.addtrack(dmktrk)
            
            if verbose:
                print('  sector ids: ', secid_list)
    
    def to_file(self, fname, tracklen):

        flags = b'\x00' if self.double_sided else b'\x10'

        with open(fname, 'wb') as f:
            hdr = b'\x00'    # 0 = normal, xff = write protect
            hdr += self.get_num_tracks().to_bytes(1, 'little')
            hdr += tracklen.to_bytes(2, 'little')
            hdr += flags
            hdr += bytes(11)
            assert len(hdr)==16, 'DMK header length not 16'
            f.write(hdr)
            
            for trk in self.tracks:
            
                idam_ptrs = b''
                
                if trk.double_density:
                    filler = b'\x4e' 
                    tdata = make_index_mark()
                    for sec in trk.sectors:
                        idam_ptr = len(tdata) + 128 + 15
                        idam_ptr |= 0x8000
                        idam_ptrs += idam_ptr.to_bytes(2, 'little')
                        tdata += make_idam(sec.track_no, sec.side, sec.id, 1)
                        tdata += make_sector(sec.data)
                else:
                    filler = b'\xFF' 
                    tdata = make_index_mark_sd()
                    for sec in trk.sectors:
                        idam_ptr = len(tdata) + 128 + 12
                        idam_ptrs += idam_ptr.to_bytes(2, 'little')
                        tdata += make_idam_sd(sec.track_no, sec.side, sec.id, 1)
                        tdata += make_sector_sd(sec.data)

                idam_ptrs = idam_ptrs.ljust(128, b'\0')
                f.write(idam_ptrs)
                
                if len(tdata) > (tracklen-128):
                    print('Error: Track data too long ({:1}) for specified track length ({:2})'.format(
                        len(tdata)+128, args.tracklen))
                    exit(1)
                
                tdata = tdata.ljust(tracklen-128, filler)
                f.write(tdata)


if __name__ == '__main__':

    parser = argparse.ArgumentParser(
            description='xml to dmk virtual disk conversion tool  (S.Orchard Oct 2023)')

    parser.add_argument('infile', help='input file')
    parser.add_argument('outfile', help='output file')
    parser.add_argument('-t', '--tracklen', type=int,
        help='specify track length in bytes', default=6400)
    parser.add_argument('-v', '--verbose',
        help='enable verbose output', action='store_true')

    args = parser.parse_args()

    if args.verbose:
        print()
        print('Input file   : {:1}'.format(args.infile))
        print('Output file  : {:1}'.format(args.outfile))
        print('Track length : {:1} bytes'.format(args.tracklen))
        print()

    dmk = DMKdisk()
    dmk.from_xml_file(args.infile, args.verbose)

    print()
    print('Writing {:1} tracks, {:2}-sided'.format(
        dmk.get_num_tracks(), ('single', 'double')[int(dmk.double_sided)]))

    dmk.to_file(args.outfile, args.tracklen)
