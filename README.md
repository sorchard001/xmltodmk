## xmltodmk.py

Disk image file conversion tool written in python.

Takes an xml file created by hxc floppy emulator software and outputs a dmk file.

### Command line options

```
xmltodmk.py [-h] [-t TRACKLEN] [-v] infile outfile
```

| Option        | Description                                  |
|:--------------|:---------------------------------------------|
|-h, --help     | Show help                                    |
|-t, --tracklen | Specify dmk track length (default 6400)      |
|-v, --verbose  | Show extra information during conversion     |
| infile        | Input xml file                               |
| outfile       | Output dmk file                              |

### Limitations

Only supports IBM FM/MFM formats.

256 byte sectors only. It's not difficult to support other sizes but I don't currently have a need for them.

### Motivation

To assist with archiving of mixed density disks.

